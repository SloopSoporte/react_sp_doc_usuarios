import React, { Component } from "react";
import { Alert, Platform, Image, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Content, Form, Item, Label, Input, Button } from "native-base";
import ImagePicker from 'react-native-image-picker';
import firebase from "react-native-firebase";
import ImageResizer from 'react-native-image-resizer';

const uri = "https://firebasestorage.googleapis.com/v0/b/sloop-dev.appspot.com/o/585e4bf3cb11b227491c339a.png?alt=media&token=ed55478f-2fe6-413c-907f-f7b231009727";

export default class Profile extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      nombre: "",
      apellidop: "",
      apellidom: "",
      correo: "",
      filePath: '',
      fotoperfil: "",
      width: "",
      height: "",
      cutfile: ""
    };
    var aux = firebase.auth().currentUser;
    this.usersRef = firebase
      .firestore()
      .collection("users")
      .doc(aux.uid);
  }
  componentDidMount() {
    this._isMounted = true;
    this.getUserInfo();
    const uid = firebase.auth().currentUser.uid;
    console.warn(uid)
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getUserInfo() {
    var aux = firebase.auth().currentUser;
    this.usersRef.onSnapshot(doc => {
      if (!doc.exists) {
        console.log("No such document!");
      } else {
        if (this._isMounted) {
          this.setState({
            Telefono: aux.phoneNumber,
            nombre: doc.data().Nombre,
            apellidop: doc.data().ApellidoPaterno,
            apellidom: doc.data().ApellidoMaterno,
            correo: doc.data().Correo,
            fotoperfil: doc.data().FotoPerfil
          });
        }
        console.log("Document data:", doc.data());
      }
    });
  }

  UpdateInfoUser = e => {
    var aux = firebase.auth().currentUser;
    e.preventDefault();
    const db = firebase.firestore();
    db.settings({
      timestampsInSnapshots: true
    });

    const userRef = db.collection("users").doc(aux.uid).update({
      Nombre: this.state.nombre,
      ApellidoPaterno: this.state.apellidop,
      ApellidoMaterno: this.state.apellidom,
      Correo: this.state.correo
    })


  }

  chooseFile = () => {
    var options = {
      title: 'Seleccionar imagen',
      takePhotoButtonTitle: "Tomar foto",
      chooseFromLibraryButtonTitle: "Elegir foto",
      cancelButtonTitle: "Cancelar",
      maxWidth: this.state.width / 8,
      maxHeight: this.state.height / 8,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      var options = {
        maxWidth: this.state.width / 8,
        maxHeight: this.state.height / 8
      }
      if (response.didCancel) {
        console.log('User cancelled image picker');
        alert('Se cancelo la eleccion');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        alert('ImagePicker Error: ' + response.error);
      } else {
        let source = response;

        this.setState({
          filePath: response.uri,
          width: response.width,
          height: response.height,

        });
        this.setState({
          fotoperfil: this.state.filePath
        })
        ImageResizer.createResizedImage(this.state.filePath, this.state.width / 8, this.state.height / 6, 'JPEG', 100, 0)
          .then(({ uri }) => {
            this.setState({
              cutfile: uri,
            });
          })
          .catch(err => {
            console.log(err);
            return Alert.alert('Unable to resize the photo', 'Check the console for full the error message');
          });
        setTimeout(() => {
          console.warn(this.state.cutfile)
          this.uploadImage(this.state.cutfile, `${uid}.jpg`)
        }, 2000);

      }
    });
  };

  launchCamera = () => {
    ImagePicker.launchCamera(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
        alert('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        alert('ImagePicker Error: ' + response.error);
      } else {
        let source = response;
      }
    });
  };

  launchLibrary = () => {
    var options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
        alert('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        alert('ImagePicker Error: ' + response.error);
      } else {
        let source = response;
      }
    });
  };

  uploadImage(uri, imageName, mime = 'application/octet-stream') {
    return new Promise((resolve, reject) => {
      const imagePath = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
      const imageRef = firebase
        .storage()
        .ref('images')
        .child(imageName);
      let mime = 'image/jpg';

      imageRef
        .put(imagePath, { contentType: mime })
        .then(() => {
          return imageRef.getDownloadURL();
        }).then(url => {
          var aux = firebase.auth().currentUser;
          const db = firebase.firestore();
          const userRef = db.collection("users").doc(aux.uid).update({
            FotoPerfil: url

          })
        })
        .catch(reject);
    })
  }

  render() {
    return (
      <Content>
        {Platform.OS === "android" ? <Text style={{ marginTop: 20 }} /> : null}
        {!this.state.fotoperfil ? <TouchableOpacity onPress={this.chooseFile.bind(this)} style={{ justifyContent: "center", alignItems: "center" }}>
          <Image style={styles.avatar} source={{ uri }} />
        </TouchableOpacity> :
          <TouchableOpacity onPress={this.chooseFile.bind(this)} style={{ justifyContent: "center", alignItems: "center" }}>
            <Image style={styles.avatar} source={{ uri: this.state.fotoperfil }} />
          </TouchableOpacity>}

        <Item
          floatingLabel
          style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
        >
          <Label>Nombre</Label>
          <Input
            value={this.state.nombre}
            onChangeText={value => this.setState({ nombre: value })}
          />
        </Item>

        <Item
          floatingLabel
          style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
        >
          <Label>Apellido Paterno</Label>
          <Input
            value={this.state.apellidop}
            onChangeText={value => this.setState({ apellidop: value })}
          />
        </Item>

        <Item
          floatingLabel
          style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
        >
          <Label>Apellido Materno</Label>
          <Input
            value={this.state.apellidom}
            onChangeText={value => this.setState({ apellidom: value })}
          />
        </Item>

        <Item
          floatingLabel
          style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
        >
          <Label>Correo</Label>
          <Input
            autoCapitalize="none"
            value={this.state.correo}
            onChangeText={value => this.setState({ correo: value })}
          />
        </Item>

        <Item
          floatingLabel
          style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
        >
          <Label>Télefono</Label>
          <Input
            autoCapitalize="none"
            value={this.state.Telefono}
          />
        </Item>

        <Button
          block
          rounded
          style={{
            marginTop: 20,
            marginLeft: 20,
            marginRight: 20,
            backgroundColor: "#212121"
          }}
          onPress={this.UpdateInfoUser}
        >
          <Text style={{ color: "white" }}>Actualizar información</Text>
        </Button>



      </Content>
    );
  }
}

const styles = StyleSheet.flatten({
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    margin: 20
  }
});