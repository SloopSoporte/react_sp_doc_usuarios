import React, { Component } from "react";
import { Text, StyleSheet, View, Image, Alert } from "react-native";
import { Content, Icon } from "native-base";
import firebase from "react-native-firebase";
import TimerMachine from 'react-timer-machine';
import moment from "moment";
import momentDurationFormatSetup from "moment-duration-format";
export default class RideStatus extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      rideStatus: "",
      stopTime: true,
    };
    this.usersRef = firebase
      .firestore()
      .collection("rides")
      .doc(this.props.idRide);
  }

  componentDidMount() {
    this._isMounted = true;
    try{
      this.getRideInfo();
    }catch(Ex){

    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getRideInfo() {
    this.usersRef.onSnapshot(doc => {
      if (!doc.exists) {
        console.log("No such document!");
      } else {
        if (this._isMounted) {
          this.setState({
            rideStatus: doc.data().rideStatus
          });
          switch (this.state.rideStatus) {
            
            case "1":
              Alert.alert("Estado en 1");
              break;
      
            case "2":
              Alert.alert("Estado en 2");
              break;
      
            case "3":
              Alert.alert("Estado en 3");
      
              break;
      
            case "4":
              Alert.alert("Estado en 4");
              break;
      
            // default:
            //   Alert.alert("NUMBER NOT FOUND");
          }
        }
        console.log("Document data:", doc.data());
      }
    });
  }

  onTimeOut = () => {
    this.setState({
      rideStatus: "5",
      stopTime: false
    })
  }

  render() {
    return (
      <Content style={{ margin: 20 }}>
        <Text
        style={{
          fontSize:20,marginBottom:10,fontWeight:"500"
        }}>Tú número de servicio es: {this.props.idRide}</Text>
        <Text
        style={{
          fontSize:20,marginBottom:10,fontWeight:"400"
        }}>Tú servicio se encuentra: {this.state.rideStatus}</Text>

        <Text
        style={{
          fontSize:20,marginBottom:10,fontWeight:"300"
        }}>
           Tú servicio se cancelará en:
        <TimerMachine
         timeStart={0 * 1000} // start at 10 seconds
         timeEnd={300 * 1000} // end at 20 seconds
         started={this.state.stopTime}
         countdown={false} // use as stopwatch
         interval={1000} // tick every 1 second
         formatTimer={(time, ms) =>
          moment.duration(ms, "milliseconds").format("h:mm:ss")
        }
         onComplete={time => 
          // Alert.alert("terminado")
         this.onTimeOut()
        }
        />
        </Text>

      </Content>
    );
  }
}

const styles = StyleSheet.flatten({});
