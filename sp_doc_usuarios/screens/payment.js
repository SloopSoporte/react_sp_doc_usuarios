import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import {  Button } from "native-base";
export default class Payment extends Component {
  render() {
    return (
      <View style={{marginTop:60}}>
       
        <CreditCardInput onChange={this._onChange} />

        <Button
          block
          rounded
          style={{
            marginTop: 20,
            marginLeft: 20,
            marginRight: 20,
            backgroundColor: "#212121"
          }}
          onPress={this.UpdateInfoUser}
        >
          <Text style={{ color: "white" }}>Agregar tarjeta</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.flatten({});