import React, { Component } from "react";
import { Container, Header, Content, Button, Icon, Text } from "native-base";
import { Dimensions, StyleSheet, ScrollView, View, Image } from "react-native";
import { Navigation } from "react-native-navigation";
import firebase from "react-native-firebase";

const window = Dimensions.get("window");

const uri ="https://firebasestorage.googleapis.com/v0/b/sloop-dev.appspot.com/o/585e4bf3cb11b227491c339a.png?alt=media&token=ed55478f-2fe6-413c-907f-f7b231009727";


export default class Menu extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      nombre: "Sloop",
      apellido: "Doc",
      fotoperfil:"",
    };
    var aux = firebase.auth().currentUser;
    this.usersRef = firebase
      .firestore()
      .collection("users")
      .doc(aux.uid);
  }
  componentDidMount() {
    this._isMounted = true;
    this.getUserInfo();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getUserInfo() {
    this.usersRef.onSnapshot(doc => {
      if (!doc.exists) {
        console.log("No such document!");
      } else {
        if (this._isMounted) {
          this.setState({
            nombre: doc.data().Nombre,
            apellido: doc.data().ApellidoPaterno,
            fotoperfil:doc.data().FotoPerfil
          });
        }
        console.log("Document data:", doc.data());
      }
    });
  }


  goToPage(Name, TopBarTittle) {
    Navigation.mergeOptions("Drawer", {
      sideMenu: {
        left: {
          visible: false
        }
      }
    });
    Navigation.push("appstack", {
      component: {
        name: Name,
        passProps: {
          text: "Pushed screen"
        },
        options: {
          topBar: {
            backButton: {
              color: "#fff"
            },
            title: {
              text: TopBarTittle,
              color: "#fff"
            },
            background: {
              color: "#10233b"
            },
            visible: true
          }
        }
      }
    });
  }

  signOutUser = async () => {
    try {
      firebase
        .auth()
        .signOut()
        .then(user => {
          Navigation.setRoot({
            root: {
              stack: {
                children: [
                  {
                    component: {
                      name: "navigation.home"
                    }
                  }
                ],
                options: {
                  statusBar: {
                    backgroundColor: "#0B2659"
                  },

                  topBar: {
                    /*title: {
                      text: 'Iniciar Sesion',
                      color: '#fff',
                      alignment: 'center'
                    },*/
                    background: {
                      color: "#0F3277"
                    }
                  }
                }
              }
            }
          });
        });
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    return (
      <View style={styles.menu}>
        <ScrollView scrollsToTop={false} style={styles.menu}>
          <View style={styles.avatarContainer}>
          {!this.state.fotoperfil ?
          <Image style={styles.avatar} source={{uri}} />
       :  
          <Image style={styles.avatar} source={{ uri: this.state.fotoperfil}} />}
            <Text style={styles.name}>
            {this.state.nombre} {this.state.apellido}
            </Text>
          </View>

          <Button
            iconLeft
            transparent
            light
            onPress={() => this.goToPage("navigation.profile", "Mi perfil")}
          >
            <Icon name="person" type="MaterialIcons" />
            <Text>Perfil</Text>
          </Button>
          <Button
            iconLeft
            transparent
            light
            onPress={() => this.goToPage("navigation.history", "Historial")}
          >
            <Icon name="folder-shared" type="MaterialIcons" />
            <Text>Servicios</Text>
          </Button>
          <Button
            iconLeft
            transparent
            light
            onPress={() => this.goToPage("navigation.historyMedical", "Historial")}
          >
            <Icon name="folder" type="MaterialIcons" />
            <Text>Historial Médico</Text>
          </Button>
          <Button
            iconLeft
            transparent
            light
            onPress={() => this.goToPage("navigation.payment", "Pago")}
          >
            <Icon name="attach-money" type="MaterialIcons" />
            <Text>Forms de pago</Text>
          </Button>
          <Button
            iconLeft
            transparent
            light
            onPress={() => this.goToPage("navigation.share", "Compartir")}
          >
            <Icon name="share" type="MaterialIcons" />
            <Text>Compartir</Text>
          </Button>
        </ScrollView>
        <Button
          iconLeft
          transparent
          light
          bordered
          onPress={this.signOutUser}
          style={{
            position: "absolute",
            marginLeft: 40,
            marginBottom: 40,
            bottom: 10
          }}
        >
          <Icon name="logout" type="SimpleLineIcons" />
          <Text>Cerrar sesión</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    width: window.width,
    height: window.height,
    backgroundColor: "#10233b",
    padding: 10
  },
  avatarContainer: {
    marginBottom: 20,
    marginTop: 20
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    flex: 1
  },
  name: {
    position: "absolute",
    left: 70,
    top: 20,
    fontSize:20,
    color: "#fff"
  },
  item: {
    fontSize: 20,
    fontWeight: "300",
    paddingTop: 5,
    color: "#fff"
  }
});
