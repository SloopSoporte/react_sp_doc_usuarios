import React, { Component } from "react";
import { Alert, TextInput, Text, StyleSheet, View, Button } from "react-native";
import firebase from "react-native-firebase";
import Geocode from "react-geocode";
import { Navigation } from "react-native-navigation";
import Mapbox from "@mapbox/react-native-mapbox-gl";
import AwesomeButton from "react-native-really-awesome-button";

// Enable or disable logs. Its optional.
Geocode.setApiKey("AIzaSyCEEwQWl0s359H8fzSoKyfrpBlQj270jxc");
Geocode.enableDebug();

export default class TestRide extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    this.state = {
      ready: false,
      Direccion: "",
      NombreCond: "",
      rideStatus: "0",
      uniqueUID: "",
      where: { lat: null, lng: null },
      date:
        date + "/" + month + "/" + year + " " + hours + ":" + min + ":" + sec
    };
  }

  componentDidMount() {
    this._isMounted = true;

    let geoOptions = {
      enableHighAccuaracy: true,
      timeOut: 20000,
      maximumAge: 60 * 60 * 24
    };
    navigator.geolocation.getCurrentPosition(
      this.geoSuccess,
      this.geoFailure,
      geoOptions
    );

    this.watchId = navigator.geolocation.watchPosition(
      position => {
        if (this._isMounted) {
          this.setState({
            where: {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            },
            ready: true,
            error: null
          });
        }
      },
      error => Alert.alert(error.message),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 60 * 60 * 24,
        distanceFilter: 10
      }
    );
  }

  geoSuccess = position => {
    //cambia el estado cuando ya esta montado el componente
    if (this._isMounted) {
      this.setState({
        ready: true,
        where: { lat: position.coords.latitude, lng: position.coords.longitude }
      });
      this.getLocationGeo();
    }
  };

  geoFailure = err => {
    if (this._isMounted) {
      this.setState({ error: err.message });
    }
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  getLocationGeo() {
    // Get address from latidude & longitude.
    Geocode.fromLatLng(this.state.where.lat, this.state.where.lng).then(
      response => {
        const address = response.results[0].formatted_address;
        console.log(address);
        //  Alert.alert(address);
        this.setState({
          Direccion: address
        });
      },
      error => {
        console.error(error);
      }
    );
  }

  async getDataDriver() {
    const markers = [];
    await firebase
      .firestore()
      .collection("doctors")
      .onSnapshot(querySnapshot => {
        querySnapshot.docs.forEach(dataCond => {
          this.setState({
            NombreCond: dataCond.data().nombre,
            Estatus: dataCond.data().status
          });
        });
      });
    return markers;
  }

  geoSuccess = position => {
    //cambia el estado cuando ya esta montado el componente
    if (this._isMounted) {
      this.setState({
        ready: true,
        where: { lat: position.coords.latitude, lng: position.coords.longitude }
      });
      this.getLocationGeo();
    }
  };

  geoFailure = err => {
    if (this._isMounted) {
      this.setState({ error: err.message });
    }
  };

  confirm = () => {
    var aux = null;
    // Alert.alert("Enviado");
    firebase
      .firestore()
      .collection("rides")
      // .doc(global.uid)
      .add({
        lat: this.state.where.lat,
        lng: this.state.where.lng,
        GeoPlaces: this.state.Direccion,
        rideStatus: "0",
        DateTime: this.state.date
      })
      .then(function(rideData) {
        Navigation.push("appstack", {
          component: {
            name: "navigation.rideStatus",
            passProps: {
              idRide: rideData.id
            },
            options: {
              topBar: {
                backButton: {
                  color: "#fff"
                },
                title: {
                  text: "Detalles",
                  color: "#fff"
                },
                background: {
                  color: "#0F3277"
                },
                visible: false
              }
            }
          }
        });
      });
  };

  accept() {
    Navigation.mergeOptions("Drawer", {
      sideMenu: {
        left: {
          visible: false
        }
      }
    });
    Navigation.push("appstack", {
      component: {
        name: "navigation.rideStatus",
        passProps: {
          idRide: rideData.id
        },
        options: {
          topBar: {
            backButton: {
              color: "#fff"
            },
            title: {
              text: "Detalles",
              color: "#fff"
            },
            background: {
              color: "#0F3277"
            }
          }
        }
      }
    });
  }

  getUserInfo() {
    this.usersRef.onSnapshot(doc => {
      if (!doc.exists) {
        console.log("No such document!");
      } else {
        if (this._isMounted) {
          this.setState({
            uniqueUID: doc.data().uid
          });
        }
        console.log("Document data:", doc.data());
      }
    });
  }

  //status
  //0 searching
  //1 accepted
  //2 in route
  //3 extra
  //4 translation
  //5 finished
  //6 cancelled

  render() {
    return (
      <View
        style={{
          flex: 1
        }}
      >
        {/* <Text
          style={{
            fontSize: 20,
            marginRight: 20,
            marginTop: 20,
            marginLeft: 20,
            fontWeight: "bold"
          }}
        >
          Iniciar viaje
        </Text>
        <Text
          style={{
            fontSize: 20,
            marginRight: 20,
            marginTop: 5,
            marginLeft: 20
          }}
        >
          rideStatus: {this.state.rideStatus}
        </Text>
        <TextInput
          placeholder="Ubicación actúal"
          value={
            this.state.where.lat
              ? String(this.state.where.lat + "," + this.state.where.lng)
              : null
          }
          style={{ marginLeft: 20, marginTop: 10, fontSize: 20 }}
        />

        <TextInput
          placeholder="Tú dirección"
          placeholderTextColor="black"
          style={{ marginLeft: 20, fontSize: 20, marginBottom: 10 }}
          value={this.state.Direccion}
        />

        <Button title="Confirmar" onPress={this.confirm} /> */}

        {this.state.ready && (
          <Mapbox.MapView
            ref={c => {
              this.mapref = c;
            }}
            styleURL={"mapbox://styles/sloop/cjb1pm13frsxv2snw4oh4rxwa"}
            zoomLevel={17}
            centerCoordinate={[this.state.where.lng, this.state.where.lat]}
            style={styles.container}
            showUserLocation={true}
            userTrackingMode={Mapbox.UserTrackingModes.Follow}
            logoEnabled={false}
            onUserLocationUpdate={this.centerMap}
            onDidFinishLoadingMap={this.centerMap}
          />
        )}

        <View
          style={{
            position: "absolute",
            marginTop: 10,
            marginLeft: 10,
            marginRight: 10
          }}
        >
          <TextInput
            placeholder="Busqueda de ubicaciones"
            placeholderTextColor="#212121"
            style={{
              fontSize: 20,
              backgroundColor: "red"
            }}
          />
        </View>

        <View style={styles.bottomView}>
          <Text
            style={{
              color: "white",
              fontWeight: "500",
              fontSize: 15
            }}
          >
            Tú ubicación: {this.state.Direccion}{" "}
          </Text>
          <Text
            style={{
              color: "#fafafa",
              fontWeight: "800",
              fontSize: 15
            }}
          >
            Tipo de servicio: Servicio Médico: {this.state.rideStatus}{" "}
          </Text>
          <Button title="Confirmar" onPress={this.confirm} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bottomView: {
    width: "100%",
    height: 100,
    backgroundColor: "#10233b",
    position: "absolute",
    bottom: 0,
    flex: 1
  },
  textStyle: {
    color: "#fff",
    fontSize: 22
  }
});
