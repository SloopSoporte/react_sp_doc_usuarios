import React, { Component } from "react";
import { View, Text, TextInput, Image, Alert, Icon } from "react-native";
import { Content, Button } from "native-base";
import firebase from "react-native-firebase";
import PhoneInput from "react-native-phone-input";
import OtpInputs from "react-native-otp-inputs";
import { Navigation } from "react-native-navigation";
import Dialog, {
  DialogFooter,
  DialogButton,
  DialogContent
} from "react-native-popup-dialog";
import SnackBar from "react-native-snackbar-component";
Navigation.registerComponent(`navigation.login`, () => LoginScreen);
import ActionButton from "react-native-action-button";

const successImageUri =
  "https://cdn.pixabay.com/photo/2015/06/09/16/12/icon-803718_1280.png";

export default class Testing extends Component {
  constructor(props) {
    super(props);
    this.unsubscribe = null;
    this.state = {
      user: null,
      message: "",
      codeInput: "",
      phoneNumber: "",
      confirmResult: null,
      visible: false,
      snackbarVisible: true
    };
  }

  componentDidMount() {
    this.unsubscribe = firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ user: user.toJSON() });
      } else {
        // User has been signed out, reset the state
        this.setState({
          user: null,
          message: "",
          codeInput: "",
          phoneNumber: "",
          confirmResult: null
        });
      }
    });
  }

  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
  }

  signIn = () => {
    if (this.state.phoneNumber.length < 13) {
      Alert.alert("Ingresa un número correcto");
    } else {
      const { phoneNumber } = this.state;
      this.setState({ message: "Enviando código..." });
      firebase
        .auth()
        .signInWithPhoneNumber(phoneNumber)
        .then(confirmResult =>
          this.setState({ confirmResult, message: "El código fue enviado, favor de ingresar" })
        )
        .catch(error =>
          this.setState({
            message: `Error al iniciar sesión: ${error.message}`
          })
        );
    }
  };

  confirmCode = () => {
    const { codeInput, confirmResult } = this.state;
    if (this.state.codeInput.length < 6) {
      Alert.alert("Ingresa un código válido por favor");
    } else{
      if (confirmResult && codeInput.length) {
        confirmResult
          .confirm(codeInput)
          .then(user => {
            this.setState({ message: "El código fue confirmado" });
            Navigation.setRoot({
              root: {
                stack: {
                  children: [
                    {
                      component: {
                        name: "navigation.map"
                      }
                    }
                  ],
                  options: {
                    statusBar: {
                      backgroundColor: "green"
                    },
  
                    topBar: {
                      /*title: {
                        text: 'Iniciar Sesion',
                        color: '#fff',
                        alignment: 'center'
                      },*/
                      background: {
                        color: "red",
                      },
                      visible: false
                    }
                  }
                }
              }
            });
          })
          .catch(error =>
            this.setState({ message: `Error al confirmar código: ${error.message}` })
          );
      }
    }
  };

  signOut = () => {
    firebase.auth().signOut();
  };


  renderPhoneNumberInput() {
    const { phoneNumber } = this.state;

    return (
      <Content style={{ padding: 25 }}>
        <Text>Ingresa tú número para continúar:</Text>
        {/* <TextInput
          autoFocus
          style={{ height: 40, marginTop: 15, marginBottom: 15 }}
          onChangeText={value => this.setState({ phoneNumber: value })}
          placeholder={'Phone number ... '}
          value={phoneNumber}
        /> */}

        <PhoneInput
          style={{ marginTop: 20 }}
          ref="phone"
          initialCountry="mx"
          onChangePhoneNumber={value => this.setState({ phoneNumber: value })}
        />
        <Button
          block
          rounded
          style={{ marginTop: 20, backgroundColor: "#10233B" }}
          onPress={this.signIn}
        >
          <Text style={{ color: "white" }}>Continúar</Text>
        </Button>

        <View style={{ marginTop: 90 }}>
          {/* <SnackBar 
        visible={true} backgroundColor="black" textMessage="Hello There!" actionHandler={()=>{console.log("snackbar button clicked!")}} actionText="let's go"/> */}
        </View>

        {/* <Button
          block
          rounded
          style={{ marginTop: 20, backgroundColor: "green" }}
          onPress={() => {
            this.setState({ visible: true });
          }}
        >
          <Text style={{ color: "white" }}>test</Text>
        </Button> */}

        <Dialog
          visible={this.state.visible}
          footer={
            <DialogFooter>
              <DialogButton text="CANCEL" onPress={() => {}} />
              <DialogButton text="OK" onPress={() => {}} />
            </DialogFooter>
          }
        >
          <DialogContent>
            <Text>hehehe</Text>
          </DialogContent>
        </Dialog>
      </Content>
    );
  }

  renderMessage() {
    const { message } = this.state;

    if (!message.length) return null;

    return (
      // <Text style={{ padding: 5, backgroundColor: "#000", color: "#fff" }}>
      //   {message}
      // </Text>
      <SnackBar
        visible={this.state.snackbarVisible}
        autoHidingTime={1000}
        position="bottom"
        bottom={0}
        backgroundColor="#10233b"
        textMessage={message}
        actionHandler={() => {
          this.setState({
            snackbarVisible: false
          });
          console.log("snackbar button clicked!");
        }}
        actionText="Ok"
      />
    );
  }

  renderVerificationCodeInput() {
    const { codeInput } = this.state;

    return (
      <View style={{marginTop:10}}>
        <Text style={{
          fontSize:20,
          color:"black",
          fontWeight:"200",
          marginLeft:20,
        }}
        >Ingresa tú código:</Text>
        {/* <TextInput
          autoFocus
          style={{ height: 40, marginTop: 15, marginBottom: 15 }}
          onChangeText={value => this.setState({ codeInput: value })}
          placeholder={"Code ... "}
          value={codeInput}
        /> */}
        <OtpInputs
          inputsContainerStyles={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'stretch',
          position:"relative",marginTop:0}}
          handleChange={value => this.setState({ codeInput: value })}
          numberOfInputs={6}
          focusedBorderColor="#212121"
          secureTextEntry={false}
        />

        <Button block rounded
        onPress={this.confirmCode}
        style={{
          marginLeft:20,marginRight:20
          ,marginTop:80,
          backgroundColor:"#0d47a1"
        }}>
          <Text
          style={{color:"white"}}>Confirmar</Text>
        </Button>
      
      </View>
    );
  }

  render() {
    const { user, confirmResult } = this.state;
    return (
      <View style={{ flex: 1 }}>
        {!user && !confirmResult && this.renderPhoneNumberInput()}

        {this.renderMessage()}

        {!user && confirmResult && this.renderVerificationCodeInput()}

        {/* {user && (
          <View
            style={{
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#77dd77',
              flex: 1,
            }}
          >
            <Image source={{ uri: successImageUri }} style={{ width: 100, height: 100, marginBottom: 25 }} />
            <Text style={{ fontSize: 25 }}>Signed In!</Text>
            <Text>{JSON.stringify(user)}</Text>
            <Button title="Sign Out" color="red" onPress={this.signOut} />
          </View>
        )} */}
      </View>
    );
  }
}
