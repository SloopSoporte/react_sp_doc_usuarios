import React, { Component } from "react";
import {
  StyleSheet,
  View,
  PermissionsAndroid,
  Platform,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  Image,
  AsyncStorage,
  TextInput,
  Animated
} from "react-native";
import {
  Header,
  Container,
  Form,
  Title,
  Item,
  Label,
  Content,
  List,
  ListItem,
  InputGroup,
  Input,
  Icon,
  Text,
  Picker,
  Button,
  Right
} from "native-base";
import firebase from "react-native-firebase";
import { Navigation } from "react-native-navigation";
import Mapbox from "@mapbox/react-native-mapbox-gl";
import { SkypeIndicator } from "react-native-indicators";
import { Col, Row, Grid } from "react-native-easy-grid";
const uri =
  "https://undoctorparati.com/wp-content/uploads/2017/09/101-doctor-1.png";
import Geocode from "react-geocode";
export default class map extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);

    this.state = {
      map_style: "mapbox://styles/sloop/cjb1pm13frsxv2snw4oh4rxwa",
      ready: false,
      where: { lat: null, lng: null },
      error: null,
      datos: false,
      visible: true,
      key1: "hehe",
      key2: "test",
      doctors: [],
      Direccion: ""
    };
  }

  timeOut() {
    setTimeout(() => {
      this.setState({ visible: false });
    }, 5000);
  }

  componentDidMount() {
    this.ServiceSend();
    this._isMounted = true;
    this.checkPermission();
    this.createNotificationListeners(); //add this line
    var user = firebase.auth().currentUser;
    firebase
      .firestore()
      .collection("users")
      .doc(user.uid)
      .get()
      .then(doc => {
        if (!doc.exists) {
          if (this._isMounted) {
            this.setState({
              visible: false
            });
          }
          //Alert.alert("No such document!");
        } else {
          if (this._isMounted) {
            this.setState({
              datos: true,
              visible: false
            });
          }
          console.log("Document data:", doc.data());
        }
      })
      .catch(err => {
        console.log("Error getting document", err);
      });
    Platform.OS === "android"
      ? PermissionsAndroid.requestMultiple(
          [
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
          ],
          {
            title: "Give Location Permission",
            message: "App needs location permission to find your position."
          }
        )
          .then(granted => {
            console.log(granted);
            resolve();
          })
          .catch(err => {
            console.warn(err);
          })
      : null;

    let geoOptions = {
      enableHighAccuaracy: true,
      timeOut: 20000,
      maximumAge: 60 * 60 * 24
    };
    navigator.geolocation.getCurrentPosition(
      this.geoSuccess,
      this.geoFailure,
      geoOptions
    );

    this.watchId = navigator.geolocation.watchPosition(
      position => {
        if (this._isMounted) {
          this.setState({
            where: {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            },
            ready: true,
            error: null
          });
        }
      },
      error => Alert.alert(error.message),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 60 * 60 * 24,
        distanceFilter: 10
      }
    );

    //this.getDrivers();
  }

  async getDrivers() {
    await firebase
      .firestore()
      .collection("doctors")
      .onSnapshot(querySnapshot => {
        let doctors = [...this.state.doctors];
        querySnapshot.docs.forEach(doctor => {
          Alert.alert(doctor.data().lattitude.toString());
          // if (
          //   this.state.ready &&
          //   doctor.data().status == "En linea" &&
          //   doctor.data().lattitude != null
          // ) {
          //calcula la distancia entre la solicitud y tu ubicacion
          // var distance = geolib.getDistance(
          //   { latitude: doctor.data().lattitude, longitude: doctor.data().longitude },
          //   {
          //     latitude: this.state.where.lat,
          //     longitude: this.state.where.lng
          //   }
          // );
          // condicional de distancia
          // if (distance < 5000) {

          // Create a new array based on current state:

          // Add item to it
          doctors.push({ lattitude: doctor.data().lattitude });
          //Alert.alert(doctor.data().longitude.toString());

          // Set state
          this.setState({ doctors });
          //}

          // }
        });
      });
  }

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log("permission rejected");
    }
  }

  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem("fcmToken");
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem("fcmToken", fcmToken);
      }
    }
    firebase
      .firestore()
      .collection("users")
      .doc(global.uid)
      .update({
        uniqueToken: fcmToken
      });
    console.log(fcmToken);
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.notificationListener();
    this.notificationOpenedListener();
  }

  async createNotificationListeners() {
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        const { title, body } = notification;
        this.showAlert(title, body);
      });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const { title, body } = notificationOpen.notification;
        this.showAlert(title, body);
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
    }
    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage(message => {
      //process data message
      console.log(JSON.stringify(message));
    });
  }

  showAlert(title, body) {
    Alert.alert(
      title,
      body,
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );
  }

  geoSuccess = position => {
    //cambia el estado cuando ya esta montado el componente
    if (this._isMounted) {
      this.setState({
        ready: true,
        where: { lat: position.coords.latitude, lng: position.coords.longitude }
      });
      this.getLocationGeo();
    }
  };

  geoFailure = err => {
    if (this._isMounted) {
      this.setState({ error: err.message });
    }
  };

  showMenu() {
    Navigation.mergeOptions("Drawer", {
      sideMenu: {
        left: {
          visible: true
        }
      }
    });
  }

  setDataUser = e => {
    var aux = firebase.auth().currentUser;
    e.preventDefault();
    const db = firebase.firestore();
    db.settings({
      timestampsInSnapshots: true
    });
    const userRef = db
      .collection("users")
      .doc(aux.uid)
      .set({
        Nombre: this.state.nombre,
        ApellidoPaterno: this.state.apellidop,
        ApellidoMaterno: this.state.apellidom,
        Correo: this.state.correo,
        lattitude: "-1",
        longitude: "-2"
      });
    this.setState({
      datos: true
    });
  };

  centerMap = () => {
    // this.locationref.reset();
    this.mapref.setCamera({
      centerCoordinate: [this.state.where.lng, this.state.where.lat],
      zoom: 16,
      duration: 2000
    });
    firebase
      .firestore()
      .collection("users")
      .doc(global.uid)
      .update({
        lattitude: this.state.where.lng,
        longitude: this.state.where.lat
      });
  };

  async ServiceSend() {
    const markers = [];
    await firebase
      .firestore()
      .collection("doctors")
      .onSnapshot(querySnapshot => {
        querySnapshot.docs.forEach(doc => {
          markers.push(doc.data());
        });
        console.log(markers);
      });
    return markers;
  }

  getLocationGeo() {
    // Get address from latidude & longitude.
    Geocode.fromLatLng(this.state.where.lat, this.state.where.lng).then(
      response => {
        const address = response.results[0].formatted_address;
        console.log(address);
        //  Alert.alert(address);
        if (this._isMounted) {
          this.setState({
            Direccion: address
          });
        }
      },
      error => {
        console.error(error);
      }
    );
  }

  goToPage(Name, TopBarTittle) {
    Navigation.mergeOptions("Drawer", {
      sideMenu: {
        left: {
          visible: false
        }
      }
    });
    Navigation.push("appstack", {
      component: {
        name: Name,
        passProps: {
          text: "Pushed screen"
        },
        options: {
          topBar: {
            backButton: {
              color: "#fff"
            },
            title: {
              text: TopBarTittle,
              color: "#fff"
            },
            background: {
              color: "#10233b"
            },
            visible: true
          }
        }
      }
    });
  }

  ridePage(Name, TopBarTittle) {
    Navigation.mergeOptions("Drawer", {
      sideMenu: {
        left: {
          visible: false
        }
      }
    });
    Navigation.push("appstack", {
      component: {
        name: Name,
        passProps: {
          text: "Pushed screen"
        },
        options: {
          topBar: {
            backButton: {
              color: "#fff"
            },
            title: {
              text: TopBarTittle,
              color: "#fff"
            },
            background: {
              color: "#10233b"
            },
            visible: false
          }
        }
      }
    });
  }

  render() {
    if (this.state.visible) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "stretch"
          }}
        >
          <SkypeIndicator size={90} color="#10233b" />
        </View>
      );
    }
    if (this.state.datos) {
      return (
        <View style={styles.container}>
          {this.state.ready && (
            <Mapbox.MapView
              ref={c => {
                this.mapref = c;
              }}
              styleURL={this.state.map_style}
              zoomLevel={14}
              centerCoordinate={[this.state.where.lng, this.state.where.lat]}
              style={styles.container}
              showUserLocation={true}
              userTrackingMode={Mapbox.UserTrackingModes.Follow}
              logoEnabled={false}
              onUserLocationUpdate={this.centerMap}
              onDidFinishLoadingMap={this.centerMap}
            >
              {/* {this.state.doctors.length > 0 ? 
              this.state.doctors.map(driver => (
                <Mapbox.PointAnnotation
                  key="1"
                  id="id1"
                  title="Test"
                  coordinate={[this.state.where.lng, driver.lattitude + .1]}
                />
              )) : null} */}
            </Mapbox.MapView>
          )}

          {/* Servicios      */}
          <Container style={styles.bottomView}>
            {/* <TextInput
              onFocus={() =>
                this.ridePage("navigation.rideStatus", "Ride Status")
              }
              placeholder="Ingresa tú ubicación"
              placeholderTextColor="white"
              style={{
                fontSize: 20,
                marginLeft: 10,
                marginBottom: 20,
                flex: 1,
                color: "white",
                fontWeight: "300"
              }}
              value={this.state.Direccion}
            />

            <Icon
              name="ios-bug"
              type="Ionicons"
              style={{
                color: "#9e9e9e",
                marginBottom: 10
              }}
              onPress={() => this.goToPage("navigation.testride", "Test")}
            /> */}
            <View
              style={{
                paddingBottom: 10,
                paddingTop: 10
              }}
            >
              <TextInput
                onFocus={() =>
                  this.ridePage("navigation.testride", "Ride Status")
                }
                placeholder="Ingresa tú ubicación"
                placeholderTextColor="white"
                value={this.state.Direccion}
                style={{
                  color:"#212121",
                  fontWeight:"600",
                  fontSize:20,
                  marginLeft:10,marginRight:10,
                }}
              />
            </View>

            <Grid
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "stretch",
                paddingRight: 0,
                paddingLeft: 20
              }}
            >
              <Col
                style={{
                  width: 120
                }}
              >
                <Button
                  style={{
                    width: 100,
                    backgroundColor:"white",
                    borderColor:"white",
                    elevation:1,
                    shadowColor:"#bdbdbd",
                    shadowOpacity: 1,
                    shadowOffset: { height: 0, width: 0 },
                  }}
                >
                  <Title
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "stretch",
                      fontSize: 16,
                      fontWeight:"100",
                      color:"#10233b"
                    }}
                  >
                    Casa
                  </Title>
                  <Icon
                    name="home"
                    type="AntDesign"
                    style={{
                      fontSize: 25,
                      color: "#10233b",
                      justifyContent: "center"
                    }}
                  />
                </Button>
              </Col>
              <Col
                style={{
                  width: 120
                }}
              >
                <Button
                  style={{
                    marginLeft:-10,
                    width: 120,
                    backgroundColor:"white",
                    borderColor:"white",
                    elevation:1,
                    shadowColor:"#bdbdbd",
                    shadowOpacity: 1,
                    shadowOffset: { height: 0, width: 0 },
                  }}
                >
                  <Title
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "stretch",
                      fontSize: 16,
                      fontWeight:"100",
                      color:"#10233b"
                    }}
                  >
                    Trabajo
                  </Title>
                  <Icon
                    name="clockcircleo"
                    type="AntDesign"
                    style={{
                      fontSize: 25,
                      color: "#10233b",
                      justifyContent: "center"
                    }}
                  />
                </Button>
              </Col>
              <Col
                style={{
                  width: 120
                }}
              >
                 <Button
                 onPress={() => this.goToPage("navigation.setRide", "Set Ride")}
                  style={{
                    width: 100,
                    backgroundColor:"#10233b",
                    borderColor:"#10233b",
                    elevation:1,
                    shadowColor:"#bdbdbd",
                    shadowOpacity: 1,
                    shadowOffset: { height: 0, width: 0 },
                  }}
                >
                  <Title
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "stretch",
                      fontSize: 16,
                      fontWeight:"100",
                      color:"#ffffff"
                    }}
                  >
                    Pedir
                  </Title>
                  <Icon
                    name="enviroment"
                    type="AntDesign"
                    style={{
                      fontSize: 25,
                      color: "#ffffff",
                      justifyContent: "center"
                    }}
                  />
                </Button>
              </Col>
            </Grid>
          </Container>

          {/*boton de menu*/}
          <TouchableOpacity
            onPress={this.showMenu}
            style={{
              borderWidth: 1,
              borderColor: "#fafafa",
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              marginTop: 40,
              marginLeft: 15,
              flex: 1,
              width: 50,
              height: 50,
              backgroundColor: "white",
              borderRadius: 100
            }}
          >
            {/* <Icon
              name="menu"
              type="Entypo"
              style={{ fontSize: 30, color: "white", justifyContent: "center" }}
            /> */}

            <Image
              source={require("../src/images/images.png")}
              style={{
                width: 50,
                height: 50,
                borderRadius: 50 / 2,
                overflow: "hidden",
                borderWidth: 1,
                borderColor:"#fafafa"
              }}
            />
          </TouchableOpacity>

          {/* <ActionButton
            ref={c => {
              this.locationref = c;
            }}
            buttonColor="#10233b"
            onPress={this.centerMap}
            renderIcon={active =>
              active ? (
                <Icon
                  name="my-location"
                  type="MaterialIcons"
                  style={{ fontSize: 30, color: "white" }}
                />
              ) : (
                <Icon
                  name="my-location"
                  type="MaterialIcons"
                  style={{ fontSize: 29, color: "white" }}
                />
              )
            }
          /> */}
        </View>
      );
    } else {
      return (
        <Content>
          {Platform.OS === "android" ? (
            <Text style={{ marginTop: 20 }} />
          ) : null}
          <View>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Image style={styles.avatar} source={{ uri }} />
              <Item floatingLabel style={{ marginLeft: 20, marginRight: 20 }}>
                <Label>Nombre</Label>
                <Input
                  onChangeText={value => this.setState({ nombre: value })}
                />
              </Item>
              <Item
                floatingLabel
                style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
              >
                <Label>Apellido paterno</Label>
                <Input
                  onChangeText={value => this.setState({ apellidop: value })}
                />
              </Item>
              <Item
                floatingLabel
                style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
              >
                <Label>Apellido materno</Label>
                <Input
                  onChangeText={value => this.setState({ apellidom: value })}
                />
              </Item>
              <Item
                floatingLabel
                style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
              >
                <Label>Correo electrónico</Label>
                <Input
                  autoCapitalize="none"
                  onChangeText={value => this.setState({ correo: value })}
                />
              </Item>
              <Button
                block
                rounded
                style={{ margin: 20 }}
                onPress={this.setDataUser}
              >
                <Text>Guardar información</Text>
              </Button>
            </View>
          </View>
        </Content>
      );
    }
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    color: "white"
  },
  container: {
    flex: 1
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    margin: 10
  },
  bottomView: {
    width: "100%",
    height: 120,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 0
  }
});
