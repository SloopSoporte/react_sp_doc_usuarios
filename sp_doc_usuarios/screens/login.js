import React, { Component } from "react";
import { Button, Text, StyleSheet, View, Image, Alert, Platform } from "react-native";
import { Content, Form, Item, Label, Input } from "native-base";
import firebase from 'react-native-firebase';
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import { Navigation } from "react-native-navigation";

const uri =
  "https://undoctorparati.com/wp-content/uploads/2017/09/101-doctor-1.png";

export default class LoginScreen extends Component {
  constructor() {
    super();
    this.state = {
      Nombre: "",
      ApellidoPaterno: "",
      ApellidoMaterno: "",
      Correo: "",
      lattitude: "-101010",
      longitude: "+111111"
    };
  }

  setDataUser = e => {
    var aux = firebase.auth().currentUser;
    e.preventDefault();
    const db = firebase.firestore();
    db.settings({
      timestampsInSnapshots: true
    });
    const userRef = db.collection("users").doc(aux.uid).set({
      Nombre: this.state.nombre,
      ApellidoPaterno: this.state.apellidop,
      ApellidoMaterno: this.state.apellidom,
      Correo: this.state.correo,
      lattitude:this.state.lattitude,
      longitude: this.state.longitude
    }).then(() => {
        Navigation.setRoot({
            root: {
              stack: {
                children: [
                  {
                    component: {
                      name: "navigation.map"
                    }
                  }
                ],
                options: {
                  statusBar: {
                    backgroundColor: "gray"
                  },

                  topBar: {
                    /*title: {
                      text: 'Iniciar Sesion',
                      color: '#fff',
                      alignment: 'center'
                    },*/
                    background: {
                      color: "red"
                    }
                  }
                }
              }
            }
          });
        // do something here
    });

  };


  render() {
    return (
      <Content>
        {Platform.OS === "android" ? <Text style={{ marginTop: 20 }} /> : null}
        <View>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Image style={styles.avatar} source={{ uri }} />
            <Item floatingLabel style={{ marginLeft: 20, marginRight: 20 }}>
              <Label>Nombre</Label>
              <Input
               onChangeText={value => this.setState({ nombre: value })} />
            </Item>
            <Item
              floatingLabel
              style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
            >
              <Label>Apellido paterno</Label>
              <Input
                onChangeText={value => this.setState({ apellidop: value })}
              />
            </Item>
            <Item
              floatingLabel
              style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
            >
              <Label>Apellido materno</Label>
              <Input
                onChangeText={value => this.setState({ apellidom: value })}
              />
            </Item>
            <Item
              floatingLabel
              style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}
            >
              <Label>Correo electrónico</Label>
              <Input autoCapitalize="none"
              onChangeText={value => this.setState({ correo: value })} />
            </Item>
        <Button title="Guardar información" onPress={this.setDataUser}>
        </Button>
          </View>
        </View>
      </Content>
    );
  }
}

const styles = StyleSheet.flatten({
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    margin: 20
  }
});
