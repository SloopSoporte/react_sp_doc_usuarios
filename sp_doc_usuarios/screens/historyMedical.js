import React, { Component } from 'react';
import { Text, StyleSheet, View, Platform, Alert } from 'react-native';
import firebase from "react-native-firebase";
import { Content, Form, Item, Picker, Button,Input, Radio, Icon, ListItem, Grid, Col, Label } from "native-base";
import DatePicker from 'react-native-datepicker'

export default class MedicalHistory extends Component {

    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            nombre: "",
            apellidop: "",
            apellidom: "",
            date: "01-01-2019",
            bloodtype: "",
            gender: "hombre",
            healthinsurance: "no",
            healthinsurancenumber: "",
            allergies: "no",
            allergiesdescription: "",
            diabetes: "no"
        };
        var aux = firebase.auth().currentUser;
        this.usersRef = firebase
            .firestore()
            .collection("users")
            .doc(aux.uid);
    }

    componentDidMount() {
        this._isMounted = true;
        this.getUserInfo();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getUserInfo() {
        var aux = firebase.auth().currentUser;
        this.usersRef.onSnapshot(doc => {
            if (!doc.exists) {
                console.log("No such document!");
            } else {
                if (this._isMounted) {
                    this.setState({
                        nombre: doc.data().Nombre,
                        apellidop: doc.data().ApellidoPaterno,
                        apellidom: doc.data().ApellidoMaterno,
                    });
                }
                console.log("Document data:", doc.data());
            }
        });




    }


    onValueChangueBlood(value) {
        this.setState({
            bloodtype: value
        });
    }
    UpdateInfoUser() {

        var aux = firebase.auth().currentUser;
        const db = firebase.firestore();
        db.settings({
            timestampsInSnapshots: true
        });
        const userRef = db.collection("users").doc(aux.uid).collection("historial").doc("datos")
        .set({
            
            Sexo: this.state.gender,
            FechaNacimiento: this.state.date,
            TipoSangre: this.state.bloodtype,
            SeguroMedico: this.state.healthinsurancenumber,
            Alergias: this.state.allergiesdescription,
            Diabetes: this.state.diabetes

        }).then(() => {

            Alert.alert(
                'Aviso',
                'Historial medico actualizado',
                [

                    { text: 'OK' },
                ],
                { cancelable: false }
            )
        })
    }

    onSubmit() {
        if (
            this.state.bloodtype == ""
        ) {
            Alert.alert(
                'Aviso',
                'Selecciona tu tipo de sangre',
                [

                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            );

        }

        else if (this.state.healthinsurance == "si" && this.state.healthinsurancenumber == "") {
            Alert.alert(
                'Aviso',
                'Escribe tu seguro medico',
                [

                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            );
        }

        else if (this.state.allergies == "si" && this.state.allergiesdescription == "") {
            Alert.alert(
                'Aviso',
                'Escribe tu o tus alergias',
                [

                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            );
        }
        else {
            this.UpdateInfoUser()
        }
    }


    render() {
        return (
            <Content>
                {Platform.OS === "android" ? <Text style={{ marginTop: 20 }} /> : null}
                <View >

                    <Grid>
                        <Col style={{ marginLeft: 20, marginTop: 30 }}>
                            {Platform.OS === "android" ?
                                <Text style={{ color: "#000", fontWeight: 'bold' }}>
                                    Nombre
            </Text>
                                :
                                <Text style={{ fontWeight: 'bold' }}>
                                    Nombre
            </Text>}

                        </Col>
                    </Grid>
                    <Grid>
                        <Col style={{ marginTop: 10, marginLeft: 20 }}>
                            {Platform.OS === "android" ?
                                <Text>{this.state.nombre + " " + this.state.apellidop + " " + this.state.apellidom}</Text>
                                :
                                <Text>{this.state.nombre + " " + this.state.apellidop + " " + this.state.apellidom}</Text>
                            }
                        </Col>
                    </Grid>

                    <Grid>
                        <Col>
                            {Platform.OS === "android" ? <Text style={{ marginTop: 10, marginLeft: 20, fontWeight: 'bold', color: "#000" }} >
                                Sexo
                                </Text> : <Text style={{ marginTop: 10, marginLeft: 20, fontWeight: 'bold' }} >
                                    Sexo
                                </Text>}

                        </Col>
                    </Grid>


                    {Platform.OS === "android" ?
                        <Grid style={{ marginTop: 10, marginLeft: 20 }}>
                            <Col style={{ width: 70 }}>
                                <Text>Hombre</Text>
                            </Col>
                            <Col style={{ width: 20 }}>
                                <Radio onPress={() => this.setState({ gender: 'hombre' })}
                                    selected={this.state.gender == 'hombre'} color={"#10223A"} selectedColor={"#10223A"} />
                            </Col>
                            <Col style={{ width: 55, marginLeft: 20 }} >
                                <Text>Mujer </Text>
                            </Col>

                            <Col>
                                <Radio onPress={() => this.setState({ gender: 'mujer' })}
                                    selected={this.state.gender == 'mujer'} color={"#10223A"} selectedColor={"#10223A"} />
                            </Col>
                        </Grid>
                        :
                        <Grid style={{ marginLeft: 5 }}>
                            <Col>
                                <ListItem noBorder onPress={() => this.setState({ gender: 'hombre' })}>
                                    <Text>Hombre </Text>
                                    <Radio selected={this.state.gender == 'hombre'} color={"#10223A"} selectedColor={"#10223A"} />
                                </ListItem>
                            </Col>
                            <Col >
                                <ListItem noBorder onPress={() => this.setState({ gender: 'mujer' })}>
                                    <Text>Mujer </Text>
                                    <Radio selected={this.state.gender == 'mujer'} color={"#10223A"} selectedColor={"#10223A"} />
                                </ListItem>

                            </Col>
                        </Grid>
                    }
                    <Grid style={{ marginTop: 10, marginLeft: 20 }}>
                        <Col>
                            {Platform.OS === "android" ? <Text style={{ fontWeight: "bold", color: "#000" }} >
                                Fecha de nacimiento
                                </Text> : <Text style={{ fontWeight: "bold" }} >
                                    Fecha de nacimiento
                                </Text>}

                        </Col>
                    </Grid>


                    <Grid style={{ marginTop: 10, marginLeft: 20 }}>
                        <Col>
                            <DatePicker
                                style={{ width: 150 }}
                                date={this.state.date}
                                mode="date"
                                placeholder="select date"
                                format="DD-MM-YYYY"
                                minDate="01-01-1920"
                                maxDate="01-01-2019"
                                confirmBtnText="Confirmar"
                                cancelBtnText="Cancelar"
                                showIcon={false}
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0,
                                    },
                                    dateInput: {
                                        borderRadius: 30,

                                    }

                                }}
                                onDateChange={(date) => { this.setState({ date: date }) }}
                            />
                        </Col>
                    </Grid>



                    <Text style={{ marginTop: 10, marginLeft: 20, fontWeight: "bold", color: "#000" }} >
                        Tipo de sangre
                                </Text>



                    {Platform.OS === "android" ? <Content style={{ marginRight: 20, marginLeft: 20 }}>

                        <Form>
                            <Item picker >

                                {Platform.OS === "android" ?
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ width: 120 }}
                                        placeholder="Selecciona tu tipo de sangre"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.bloodtype}
                                        onValueChange={this.onValueChangueBlood.bind(this)}
                                    >
                                        <Picker.Item label="Selecciona tu tipo de sangre" value="" />
                                        <Picker.Item label="A+" value="A+" />
                                        <Picker.Item label="A-" value="A-" />
                                        <Picker.Item label="B+" value="B+" />
                                        <Picker.Item label="B-" value="B-" />
                                        <Picker.Item label="AB+" value="AB+" />
                                        <Picker.Item label="AB-" value="AB-" />
                                        <Picker.Item label="O+" value="O+" />
                                        <Picker.Item label="O-" value="O-" />
                                    </Picker>
                                    :
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ width: undefined }}
                                        placeholder="Selecciona tu tipo de sangre"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.bloodtype}
                                        onValueChange={this.onValueChangueBlood.bind(this)}
                                    >
                                        <Picker.Item label="A+" value="A+" />
                                        <Picker.Item label="A-" value="A-" />
                                        <Picker.Item label="B+" value="B+" />
                                        <Picker.Item label="B-" value="B-" />
                                        <Picker.Item label="AB+" value="AB+" />
                                        <Picker.Item label="AB-" value="AB-" />
                                        <Picker.Item label="O+" value="O+" />
                                        <Picker.Item label="O-" value="O-" />
                                    </Picker>
                                }



                            </Item>
                        </Form>

                    </Content>

                        :

                        <Content>

                            <Form>
                                <Item picker >
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ width: (Platform.OS === 'ios') ? undefined : 120 }}
                                        placeholder="Selecciona tu tipo de sangre"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.bloodtype}
                                        onValueChange={this.onValueChangueBlood.bind(this)}
                                    >
                                        <Picker.Item label="A+" value="A+" />
                                        <Picker.Item label="A-" value="A-" />
                                        <Picker.Item label="B+" value="B+" />
                                        <Picker.Item label="B-" value="B-" />
                                        <Picker.Item label="AB+" value="AB+" />
                                        <Picker.Item label="AB-" value="AB-" />
                                        <Picker.Item label="O+" value="O+" />
                                        <Picker.Item label="O-" value="O-" />
                                    </Picker>
                                </Item>
                            </Form>

                        </Content>
                    }






                    <Grid>
                        <Col>
                            {Platform.OS === "android" ? <Text style={{ marginTop: 10, marginLeft: 20, fontWeight: 'bold', color: "#000" }} >
                                Seguro medico
                                </Text> : <Text style={{ marginTop: 10, marginLeft: 20, fontWeight: 'bold' }} >
                                    Seguro medico
                                </Text>}

                        </Col>
                    </Grid>


                    {Platform.OS === "android" ?
                        <Grid style={{ marginTop: 10, marginLeft: 20 }}>
                            <Col style={{ width: 30 }}>
                                <Text>Si</Text>
                            </Col>
                            <Col style={{ width: 20 }}>
                                <Radio onPress={() => this.setState({ healthinsurance: 'si' })}
                                    selected={this.state.healthinsurance == 'si'} color={"#10223A"} selectedColor={"#10223A"} />
                            </Col>
                            <Col style={{ width: 30, marginLeft: 20 }} >
                                <Text>No </Text>
                            </Col>

                            <Col style={{ width: 20 }}>
                                <Radio onPress={() => this.setState({ healthinsurance: 'no' })}
                                    selected={this.state.healthinsurance == 'no'} color={"#10223A"} selectedColor={"#10223A"} />
                            </Col>
                        </Grid>
                        :
                        <Grid style={{ marginLeft: 5 }}>
                            <Col>
                                <ListItem noBorder onPress={() => this.setState({ healthinsurance: 'si' })}>
                                    <Text>Si </Text>
                                    <Radio selected={this.state.healthinsurance == 'si'} color={"#10223A"} selectedColor={"#10223A"} />
                                </ListItem>
                            </Col>
                            <Col >
                                <ListItem noBorder onPress={() => this.setState({ healthinsurance: 'no' })}>
                                    <Text>No</Text>
                                    <Radio selected={this.state.healthinsurance == 'no'} color={"#10223A"} selectedColor={"#10223A"} />
                                </ListItem>

                            </Col>
                        </Grid>
                    }


                    {this.state.healthinsurance == 'si' ? <Item
                        floatingLabel
                        style={{ marginLeft: 20, marginRight: 20 }}
                    >
                        <Label>Ingrese su seguro medico</Label>
                        <Input
                            maxLength={20}
                            value={this.state.healthinsurancenumber}
                            onChangeText={value => this.setState({ healthinsurancenumber: value })}
                        />
                    </Item> : null}




                    <Grid>
                        <Col>
                            {Platform.OS === "android" ? <Text style={{ marginTop: 10, marginLeft: 20, fontWeight: 'bold', color: "#000" }} >
                                Alergias
                                </Text> : <Text style={{ marginTop: 10, marginLeft: 20, fontWeight: 'bold' }} >
                                    Alergias
                                </Text>}

                        </Col>
                    </Grid>


                    {Platform.OS === "android" ?
                        <Grid style={{ marginTop: 10, marginLeft: 20 }}>
                            <Col style={{ width: 30 }}>
                                <Text>Si</Text>
                            </Col>
                            <Col style={{ width: 20 }}>
                                <Radio onPress={() => this.setState({ allergies: 'si' })}
                                    selected={this.state.allergies == 'si'} color={"#10223A"} selectedColor={"#10223A"} />
                            </Col>
                            <Col style={{ width: 30, marginLeft: 20 }} >
                                <Text>No </Text>
                            </Col>

                            <Col style={{ width: 20 }}>
                                <Radio onPress={() => this.setState({ allergies: 'no' })}
                                    selected={this.state.allergies == 'no'} color={"#10223A"} selectedColor={"#10223A"} />
                            </Col>
                        </Grid>
                        :
                        <Grid style={{ marginLeft: 5 }}>
                            <Col>
                                <ListItem noBorder onPress={() => this.setState({ allergies: 'si' })}>
                                    <Text>Si </Text>
                                    <Radio selected={this.state.allergies == 'si'} color={"#10223A"} selectedColor={"#10223A"} />
                                </ListItem>
                            </Col>
                            <Col >
                                <ListItem noBorder onPress={() => this.setState({ allergies: 'no' })}>
                                    <Text>No</Text>
                                    <Radio selected={this.state.allergies == 'no'} color={"#10223A"} selectedColor={"#10223A"} />
                                </ListItem>

                            </Col>
                        </Grid>
                    }

                    {this.state.allergies == 'si' ? <Item
                        floatingLabel
                        style={{ marginLeft: 20, marginRight: 20 }}
                    >
                        <Label>Ingrese sus alergias</Label>
                        <Input
                            maxLength={20}
                            value={this.state.allergiesdescription}
                            onChangeText={value => this.setState({ allergiesdescription: value })}
                        />
                    </Item> :
                        null
                    }

                    <Grid>
                        <Col>
                            {Platform.OS === "android" ? <Text style={{ marginTop: 10, marginLeft: 20, fontWeight: 'bold', color: "#000" }} >
                                Diabetes
                                </Text> : <Text style={{ marginTop: 10, marginLeft: 20, fontWeight: 'bold' }} >
                                    Diabetes
                                </Text>}

                        </Col>
                    </Grid>


                    {Platform.OS === "android" ?
                        <Grid style={{ marginTop: 10, marginLeft: 20 }}>
                            <Col style={{ width: 30 }}>
                                <Text>Si</Text>
                            </Col>
                            <Col style={{ width: 20 }}>
                                <Radio onPress={() => this.setState({ diabetes: 'si' })}
                                    selected={this.state.diabetes == 'si'} color={"#10223A"} selectedColor={"#10223A"} />
                            </Col>
                            <Col style={{ width: 30, marginLeft: 20 }} >
                                <Text>No </Text>
                            </Col>

                            <Col style={{ width: 20 }}>
                                <Radio onPress={() => this.setState({ diabetes: 'no' })}
                                    selected={this.state.diabetes == 'no'} color={"#10223A"} selectedColor={"#10223A"} />
                            </Col>
                        </Grid>
                        :
                        <Grid style={{ marginLeft: 5 }}>
                            <Col>
                                <ListItem noBorder onPress={() => this.setState({ diabetes: 'si' })}>
                                    <Text>Si </Text>
                                    <Radio selected={this.state.diabetes == 'si'} color={"#10223A"} selectedColor={"#10223A"} />
                                </ListItem>
                            </Col>
                            <Col >
                                <ListItem noBorder onPress={() => this.setState({ diabetes: 'no' })}>
                                    <Text>No</Text>
                                    <Radio selected={this.state.diabetes == 'no'} color={"#10223A"} selectedColor={"#10223A"} />
                                </ListItem>

                            </Col>
                        </Grid>
                    }

                    <Button
                        block
                        rounded
                        style={{
                            marginTop: 20,
                            marginBottom: 20,
                            marginLeft: 20,
                            marginRight: 20,
                            backgroundColor: "#212121"
                        }}
                        onPress={() => {
                            this.onSubmit()
                        }}
                    ><Text style={{ color: "white" }}>Guardar</Text>
                    </Button>
                </View>
            </Content>

        );
    }
}

const styles = StyleSheet.flatten({});