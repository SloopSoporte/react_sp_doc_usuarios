/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
import React, { Component } from "react";
import firebase from "react-native-firebase";
import { Navigation } from "react-native-navigation";
import Mapbox from "@mapbox/react-native-mapbox-gl";
import { Alert, AsyncStorage } from "react-native";

import MapScreen from "./screens/map";
import Home from "./screens/home";
import LoginScreen from "./screens/login";

Mapbox.setAccessToken(
  "sk.eyJ1Ijoic2xvb3AiLCJhIjoiY2p0bWFzMTQ5MTE5MDQ5cGd3a3NrdGt1NyJ9.gAHELZqUsSmN-4ZApCbNAA"
);

export default class App extends Component {
  _isMounted = false;
  constructor() {
    super();
    this.state = {
      loading: true,
      datos: false
    };
  }
  /**
   * When the App component mounts, we listen for any authentication
   * state changes in Firebase.
   * Once subscribed, the 'user' parameter will either be null
   * (logged out) or an Object (logged in)
   */

  componentWillMount() {
    this._isMounted = false;
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyCqbQm_Rvmgva-6atXmAagFPE6dyh-CKBE",
      authDomain: "sloop-dev.firebaseapp.com",
      databaseURL: "https://sloop-dev.firebaseio.com",
      projectId: "sloop-dev",
      storageBucket: "sloop-dev.appspot.com",
      messagingSenderId: "784544137746"
    };
    //firebase.initializeApp(config);
  }

  componentDidMount() {
    this._isMounted = true;
    this.authSubscription = firebase.auth().onAuthStateChanged(user => {
      if (user) {
        global.uid = user.uid;
        this.usersRef = firebase
          .firestore()
          .collection("users")
          .doc(user.uid);
        this.getUserInfo();
      }
      this.setState({
        loading: false,
        user
      });
    });
  }

 

  getUserInfo = () => {
    var aux = firebase.auth().currentUser;
    this.usersRef
      .get()
      .then(doc => {
        if (!doc.exists) {
          console.log("No such document!");
        } else {
          if (this._isMounted && doc.data().nombre != null) {
            this.setState({
              datos: true
            });
          }
          console.log("Document data:", doc.data());
        }
      })
      .catch(err => {
        console.log("Error getting document", err);
      });
  };

  /**
   * Don't forget to stop listening for authentication state changes
   * when the component unmounts.
   */
  componentWillUnmount() {
    this.authSubscription();
  }

  render() {
    if (this.state.user) {
      if (this.state.datos) {
        Navigation.setRoot({
          root: {
            sideMenu: {
              //id: "sideMenu",
              left: {
                component: {
                  id: "Drawer",
                  name: "navigation.menu"
                }
              },
              center: {
                stack: {
                  id: "appstack",
                  children: [
                    {
                      component: {
                        name: "navigation.login"
                      }
                    }
                  ],
                  options: {
                    topBar: {
                      background: {
                        color: "#10233b"
                      },
                      visible: false,
                      style: "light",
                      drawBehind: true
                    }
                  }
                }
              }
            }
          }
        });
        return <LoginScreen />;
      } else {
        Navigation.setRoot({
          root: {
            sideMenu: {
              //id: "sideMenu",
              left: {
                component: {
                  id: "Drawer",
                  name: "navigation.menu"
                }
              },
              center: {
                stack: {
                  id: "appstack",
                  children: [
                    {
                      component: {
                        name: "navigation.map"
                      }
                    }
                  ],
                  options: {
                    topBar: {
                      background: {
                        color: "#10233b"
                      },
                      visible: false,
                      style: "light",
                      drawBehind: true
                    }
                  }
                }
              }
            }
          }
        });
        return <MapScreen />;
      }
    } else {
      Navigation.setRoot({
        root: {
          stack: {
            children: [
              {
                component: {
                  id: "appHome",
                  name: "navigation.home"
                }
              }
            ],
            options: {
              statusBar: {
                backgroundColor: "#10233b"
              },

              topBar: {
                /*title: {
                  text: 'Iniciar Sesion',
                  color: '#fff',
                  alignment: 'center'
                },*/
                background: {
                  color: "#10233b"
                }
              }
            }
          }
        }
      });
      return <Home />;
    }

    // The user is null, so they're logged out
  }
}
