/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import { Navigation } from "react-native-navigation";
import App from './App';


import MapScreen from "./screens/map";
import LoginScreen from "./screens/login";
import Menu from "./screens/menu";
import Profile from "./screens/profile";
import Home from "./screens/home";
import History from "./screens/history";
import HistoryMedical from "./screens/historyMedical";
import Payment from "./screens/payment";
import Share from "./screens/share";
import SetRide from "./screens/setRide";
import RideStatus from "./screens/rideStatus";



Navigation.registerComponent(`navigation.login`, () => LoginScreen);
Navigation.registerComponent(`navigation.map`, () => MapScreen);
Navigation.registerComponent(`navigation.menu`, () => Menu);
Navigation.registerComponent(`navigation.profile`, () => Profile);
Navigation.registerComponent(`navigation.home`, () => Home);
Navigation.registerComponent(`navigation.history`, () => History);
Navigation.registerComponent(`navigation.historyMedical`, () => HistoryMedical);
Navigation.registerComponent(`navigation.payment`, () => Payment);
Navigation.registerComponent(`navigation.share`, () => Share);
Navigation.registerComponent(`navigation.setRide`, () => SetRide);
Navigation.registerComponent(`navigation.rideStatus`, () => RideStatus);


Navigation.registerComponent(`navigation.playground.WelcomeScreen`, () => App);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      component: {
        name: "navigation.playground.WelcomeScreen"
      }
    }
  });
})
